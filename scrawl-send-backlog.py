#!/usr/bin/python

from scrawl import scrawl

import os, errno

if __name__ == "__main__":

    import sys
    sys.path.insert(0, '/usr/libexec/scrawl')
    import config
    scrawl_host = config.scrawl['host']
    scrawl_port = config.scrawl['port']

    scrawl_backlog_dir = '/var/spool/scrawl/backlog'

    dirList = os.listdir(scrawl_backlog_dir)

    for category in dirList:
        #print str(category)
        try:
            os.rmdir(scrawl_backlog_dir + '/' + category)
        except OSError as e:
            if e.errno == errno.ENOTEMPTY:
                print "directory not empty"

        scrawl_spool_dir_file = scrawl_backlog_dir + '/' + category + '/' + category + '_00000'
        if os.path.isfile(scrawl_spool_dir_file):
            if os.stat(scrawl_spool_dir_file).st_size == 0:
                os.unlink(scrawl_spool_dir_file)
            else:
                print str('scrawl_spool_dir_file ' + scrawl_spool_dir_file)
                print 'proto send scrawl ' + str(scrawl_host) + ' port ' + str(scrawl_port)
                drain = scrawl.ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack(scrawl_spool_dir_file, scrawl_host, scrawl_port)
                send = drain.send()
                print str(send)


