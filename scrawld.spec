
%define bindir  /usr/bin
%define sbindir /usr/sbin

Summary: Scrawl Server Daemon and Client
Name: scrawld
Version: v3.0
Release: 0%{?dist}
License: GPL
URL: https://gitlab.com/cacography/scrawl/-/archive/master/scrawl-master.tar.gz
Group: Applications/Internet
Source0: scrawl-%{version}.tar.gz

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch

%if 0%{?rhel} == 7
AutoReqProv: no
Requires: python
%endif

%if 0%{?rhel} == 6
# 100% cpu usage when using asyncore with UNIX socket
# https://bugs.python.org/issue12502
AutoReqProv: no
Requires: python27
%endif

Requires(pre): /usr/sbin/useradd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel

Provides: scrawld

%description
Server scrawld daemon

%prep
tar xzvf %{SOURCE0}

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{bindir}
 
cp scrawl-%{version}/scrawld.py $RPM_BUILD_ROOT/%{bindir}/scrawld
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawld

cp scrawl-%{version}/client.py $RPM_BUILD_ROOT/%{bindir}/scrawl
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl

cp scrawl-%{version}/scrawl-backlog.py $RPM_BUILD_ROOT/%{bindir}/scrawl-backlog
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl-backlog

cp scrawl-%{version}/scrawl-send-backlog.py $RPM_BUILD_ROOT/%{bindir}/scrawl-send-backlog
chmod 755 $RPM_BUILD_ROOT/%{bindir}/scrawl-send-backlog

mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.py

cp scrawl-%{version}/scrawltcp_tail.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawltcp_tail.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawltcp_tail.py

#mkdir -p $RPM_BUILD_ROOT/etc
#cp scrawl-%{version}/scrawl.conf $RPM_BUILD_ROOT/etc/

%if 0%{?rhel} == 7
mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.7/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/lib/systemd/system
cp scrawl-%{version}/scrawld.service $RPM_BUILD_ROOT/lib/systemd/system/scrawld.service
%endif

%if 0%{?rhel} == 6

#mkdir -p $RPM_BUILD_ROOT/etc/profile.d
#cp scrawl-%{version}/rh6/rh6.python27.env.txt $RPM_BUILD_ROOT/etc/profile.d/python27.sh

#/opt/rh/python27/root/usr/lib64/python2.7/site-packages
#/usr/lib/python2.6/site-packages

mkdir -p $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/usr/lib/python2.6/site-packages/scrawl/scrawl.py

mkdir -p $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl
cp scrawl-%{version}/scrawl/__init__.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.py
cp scrawl-%{version}/scrawl/scrawl.py $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.py

touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.pyc
touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.pyo

touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.pyc
touch $RPM_BUILD_ROOT/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.pyo

#mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl-send-backlog.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl-send-backlog.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld_tcp.py
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld_tcp.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/config.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/config.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/scrawl.pyo

touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.pyc
touch $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl/__init__.pyo

mkdir -p $RPM_BUILD_ROOT/etc/init.d
cp scrawl-%{version}/scrawld.init $RPM_BUILD_ROOT/etc/init.d/scrawld
chmod 755 $RPM_BUILD_ROOT/etc/init.d/scrawld

%endif


mkdir -p $RPM_BUILD_ROOT/usr/libexec/scrawl
mkdir -p $RPM_BUILD_ROOT/var/spool/scrawl
mkdir -p $RPM_BUILD_ROOT/var/lib/scrawl
mkdir -p $RPM_BUILD_ROOT/var/log/scrawl

cp scrawl-%{version}/scrawld.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld.py

cp scrawl-%{version}/scrawld_tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld_tcp.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawld_tcp.py

cp scrawl-%{version}/client.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.py

cp scrawl-%{version}/client-tcp.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl-tcp.py
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl-tcp.py

cp scrawl-%{version}/config.py $RPM_BUILD_ROOT/usr/libexec/scrawl/config.py
chmod 644 $RPM_BUILD_ROOT/usr/libexec/scrawl/config.py

cp scrawl-%{version}/client.php $RPM_BUILD_ROOT/usr/libexec/scrawl/client.php
chmod 755 $RPM_BUILD_ROOT/usr/libexec/scrawl/client.php

cp scrawl-%{version}/scrawl.php $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawl.php

cp scrawl-%{version}/scrawltcp_tail.py $RPM_BUILD_ROOT/usr/libexec/scrawl/scrawltcp_tail.py


%clean
rm -rf $RPM_BUILD_ROOT

%pre
# Add user/group here if needed...
echo "Add user/group here if needed..." >/dev/null 2>&1
/usr/bin/getent group scrawl > /dev/null || /usr/sbin/groupadd -r scrawl
/usr/bin/getent passwd scrawl > /dev/null || /usr/sbin/useradd -r -d /usr/libexec/scrawl -s /sbin/nologin -g scrawl scrawl

%post
# Add serivces for startup
%if 0%{?rhel} == 6
  echo "rh6"
  echo "install /etc/init.d/scrawld"
       /sbin/chkconfig --add scrawld 
  if [ $1 = 1 ]; then #1 install
    echo "start scrawld"
        /etc/init.d/scrawld start
  else
    echo "restart scrawld"
        /etc/init.d/scrawld restart
  fi
%endif

%if 0%{?rhel} == 7
  echo "systemctl daemon-reload"
        systemctl daemon-reload
  if [ $1 = 1 ]; then #1 install
    echo "systemctl enable scrawld"
          systemctl enable scrawld
    mkdir -p /var/spool/scrawl/backlog >/dev/null 2>&1
    chown scrawl /var/spool/scrawl/backlog >/dev/null 2>&1
    echo "systemctl start scrawld"
          systemctl start scrawld
  else
    echo "systemctl restart scrawld"
          systemctl restart scrawld
  fi
%endif

ln -s /usr/libexec/scrawl/config.py /etc/scrawl.conf
#end post

%postun
rm -f /etc/scrawl.conf

%files
%defattr(-,root,root)
%{bindir}/scrawld
%{bindir}/scrawl
%{bindir}/scrawl-backlog
%{bindir}/scrawl-send-backlog

%if 0%{?rhel} == 7
/usr/lib/python2.7/site-packages/scrawl/__init__.py
/usr/lib/python2.7/site-packages/scrawl/scrawl.py
/lib/systemd/system/scrawld.service
%endif

%if 0%{?rhel} == 6
/usr/lib/python2.6/site-packages/scrawl/__init__.py
/usr/lib/python2.6/site-packages/scrawl/scrawl.py
/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/__init__.py
/opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/scrawl.py
/etc/init.d/scrawld
#/etc/profile.d/python27.sh
%endif

%dir /usr/libexec/scrawl
%dir /usr/libexec/scrawl/scrawl
%dir %attr(0755, scrawl, scrawl) /var/lib/scrawl
%dir %attr(0755, scrawl, scrawl) /var/spool/scrawl
%dir %attr(0755, scrawl, scrawl) /var/log/scrawl
%config(noreplace) /usr/libexec/scrawl/config.py
/usr/libexec/scrawl/scrawl/__init__.py
/usr/libexec/scrawl/scrawl/scrawl.py
/usr/libexec/scrawl/scrawl.py
/usr/libexec/scrawl/scrawl-tcp.py
/usr/libexec/scrawl/client.php
/usr/libexec/scrawl/scrawl.php
/usr/libexec/scrawl/scrawld.py
/usr/libexec/scrawl/scrawld_tcp.py
/usr/libexec/scrawl/scrawltcp_tail.py

#/usr/libexec/scrawl/scrawltcp_tail
#%attr(0755, scrawl, scrawl) /usr/libexec/scrawl/purge.backlog.py

%if 0%{?rhel} == 7
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.7/site-packages/scrawl/*.pyo
%endif

%if 0%{?rhel} == 6
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyc
%exclude /usr/lib/python2.6/site-packages/scrawl/*.pyo
%exclude /opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/*.pyc
%exclude /opt/rh/python27/root/usr/lib64/python2.7/site-packages/scrawl/*.pyo
%endif

%exclude /usr/libexec/scrawl/scrawl/*.pyc
%exclude /usr/libexec/scrawl/scrawl/*.pyo
%exclude /usr/libexec/scrawl/*.pyc
%exclude /usr/libexec/scrawl/*.pyo

%changelog
* Sat Aug 24 2019 Karl Rink <karl@rink.us> v3.0-0
- .gitlab-ci.yml

* Mon Mar 18 2019 Karl Rink <karl@usaepay.com> v3.0-1
- purged scribe

* Wed May 16 2018 Karl Rink <karl@usaepay.com> v2.5.0.0.0-2
- scrawld, rh6 updates for startup init scripts

* Wed May 16 2018 Karl Rink <karl@usaepay.com> v2.5.0.0.0-1
- separate/split scrawld (scribe libs) from scrawl_log

* Sat Apr 21 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.8-2
- scrawl_log

* Thu Jan 25 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.8
- _00000

* Wed Jan 24 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.7-2
- rh6 re-pkg, carry scribe and thrift python27 site-packages

* Tue Jan 23 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.7-1
- rh6 compat/install

* Sun Jan 21 2018 Karl Rink <karl@usaepay.com> v2.4.0.0.6-1
- v2.4 header/protocol

* Mon Jan 08 2018 Karl Rink <karl@usaepay.com> v2.3.0.0.9-1
- initial rpm

