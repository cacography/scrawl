#!/usr/bin/php
<?php

require_once 'scrawl.php';
define('socket', '/var/lib/scrawl/scrawld.sock');

$category = 'php_testing scrawl 123';
$message  = 'This is a test of the general php client!';

$scrawl = new SCRAWL();
$result = $scrawl->send(socket, $category, $message);
//$result = $scrawl->sendtcp(host, port, $category, $message);
if ($result != '6') { //#ACK chr(6) (acknowledge)
    echo 'fail ' . $result . PHP_EOL;
} else {
    echo 'success ' . PHP_EOL;
}

?>
