<?php

/* SCRAWL Class
 *
 */

/*
#NUL chr(0)
#SOH chr(1) (start of heading)
#STX chr(2) (start of text)
#ETX chr(3) (end of text)
#EOT chr(4) (end of transmission)
#ENQ chr(5) (enquiry)
#ACK chr(6) (acknowledge)
#NAK chr(21) (negative acknowledge)
*/

class SCRAWL
{

  public function send($socket_file, $category, $message ) {

    $socket = socket_create(AF_UNIX, SOCK_STREAM, 0);
    if ($socket === false) {
        echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        exit(1);
    }
    //} else {
    //    echo "socket_create \n";
    //}

    //echo "Connect to " . $socket_file . "\n";
    $result = socket_connect($socket, $socket_file);
    if ($result === false) {
        echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
        exit(1);
    }
    //} else {
    //    echo "socket_connect \n";
    //}

    //echo "Read Server...\n";
    //$hello = socket_read($socket, 1024);
    $hello = socket_read($socket, 1);
    //if ($hello != 'scrawl') {
    if ($hello != '1') { //#SOH chr(1)  (start of heading)
        echo "NO SOH, start of heading SCRAWL \n";
        exit(1);
    }

    define('flag_log',1);
    define('flag_gzip',2);
    define('flag_bzip',4);
    define('flag_cipher1',5);
    define('flag_cipher2',6);
    define('flag_jumbo',8);

    //echo "Set Header...\n";
    $flag = '1';

    // limit category to 255 length
    $category = substr($category, 0, 255);

    // category length
    $catlen = strlen($category);
    //echo "catlen is " . $catlen . "\n";

    // chr($catlen);
    $catbyte = chr($catlen);
    //echo "catbyte is " . $catbyte . "\n";

    // message length
    $msglen = strlen($message);
    //echo "msglen is " . $msglen . "\n";

    $msgbytes = chr(floor($msglen/256)) . chr($msglen%256);
    //echo "msgbytes is " . $msgbytes . "\n";

    $header = $flag . $catbyte . $msgbytes;
    //echo 'Header is ' . $header . "\n";

    $msg = $header . $category . $message;
    //echo "msg is " . $msg;

    //echo "Send data...\n";
    $success = "None";
    socket_write($socket, $msg, strlen($msg));

    //echo "Reading response:\n";
    $str = '';
    //while ($resp = socket_read($socket, 1024)) {
    while ($resp = socket_read($socket, 1)) {
        $str .= $resp;
        if (strpos($str, "6") !== false) { //#ACK chr(6) (acknowledge)
            //echo "Response: " . $str . "\n";
            $success = "6";
            break;
        }
    }

    socket_close($socket);
    //echo "Closed Socket\n";

    return $success;
  }

}

?>
