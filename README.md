
scrawl v3
=========

Scrawl is a server for aggregating log data streamed in real-time from a large number of servers.    

Scrawl is a replacement for the now extinct scribe logging server from facebook https://en.wikipedia.org/wiki/Scribe_(log_server)  Scrawl is written in python and initially supported (scrawl v1) the scribe protocol during a migration period of old servers and services with dependencies on the facebook scribe protocol.  Today, scrawl v3, is fully independent of the scribe protocol but retains heart of what scribe was and provided; the auto category generation, network topology, and scalability of the service.



Install
--------

clone the source:
```
git archive --format=tar --remote=git@gitlab.com:cacography/scrawl.git --prefix=scrawl-v3.0/ master | gzip > scrawl-v3.0.tgz
```

or, install via rpm:
```
rpm -ivh https://gitlab.com/cacography/scrawl/raw/master/scrawld-v3.0-0.el7.noarch.rpm
```

```
[krink@centos7 ~]$ sudo rpm -ivh https://gitlab.com/cacography/scrawl/raw/master/scrawld-v3.0-0.el7.noarch.rpm
Retrieving https://gitlab.com/cacography/scrawl/raw/master/scrawld-v3.0-0.el7.noarch.rpm
Preparing...                          ################################# [100%]
Updating / installing...
   1:scrawld-v3.0-0.el7               ################################# [100%]
systemctl daemon-reload
systemctl enable scrawld
Created symlink from /etc/systemd/system/multi-user.target.wants/scrawld.service to /usr/lib/systemd/system/scrawld.service.
systemctl start scrawld
[krink@centos7 ~]$
```
Scrawl was originally written and tested on RedHat 6 (technically CentOS), works great on RedHat 7, and MacOS works as well.

Service
-----

The local daemon (scrawld) will have a unix socket located at /var/lib/scrawl/scrawld.sock   The unix socket accepts application logging from any client that can speak the scrawl protocol.  Any messages caputured by the unix socket is relayed upstream to the configured remote logging server.  If a remote server is not available or goes off-line, messages are spooled locally untill the upstream service becomes available.  Originally, local applications would send events to the local unix socket via scrawl protocol, and the scrawl daemon would then send those messages upstream to a scribe server via the scribe protocol.

Configuration
-------------

On RedHat/CentOS machines, the /etc/scrawl.conf is a symlink to /usr/libexec/scrawl/config.py   This is the configuration file that dictates where and what port the remote server is located.  Originally in scrawl v2, it had the ability to send to both scrawl and scribe.  Today, only scrawl.

/etc/scrawl.conf
```
scrawl = dict(
  socket = '/var/lib/scrawl/scrawld.sock',
  host = 'remote-host-or-ip',
  port = 1201
)

```


Usage (client)
-----

To send a message or capture an error, the application simply needs to use the scrawl client.

Python
```
scrawl-v3.0/client.py
```
```
>>> from scrawl import scrawl
>>> category = 'TESTING Python scrawl 321'
>>> message  = 'yep, this is a message or event...'
>>> client = scrawl.ScrawlSocketClient("/var/lib/scrawl/scrawld.sock")
>>> result = client.send(category, message)
>>> print(result)
True

```

PHP
```
scrawl-v3.0/client.php
scrawl-v3.0/scrawl.php
```
```
<?php

require_once 'scrawl.php';
define('socket', '/var/lib/scrawl/scrawld.sock');

$category = 'TESTING PHP scrawl 321';
$message  = 'This is a test of the scrawl php client!';

$scrawl = new SCRAWL();
$result = $scrawl->send(socket, $category, $message);
if ($result != '6') { 
    echo 'fail ' . $result . PHP_EOL;
} else {
    echo 'success ' . PHP_EOL;
}

?>

```

You can also send directly via tcp (skipping the unix socket), and if the service isn't working or ready, you'll get a return of 'False' 

Python
```
scrawl-v3.0/client-tcp.py
```
```
>>> from scrawl import scrawl
>>> category = 'TESTING Python ScrawlTCPClient'
>>> message  = 'a ScrawlTCPClient message via tcp, not via unix socket...'
>>> client = scrawl.ScrawlTCPClient('localhost', 1201)
>>> result = client.send(category, message)
Connection error: [Errno 111] Connection refused localhost:1201
>>> print(result)
False

```


Server (remote)
---------------

The remote service simply listens on a tcp port and accepts messages from clients that can speak the scrawl protocol.  Once a client has connected to the remove server via the scrawl protocol, the message is written to the destination category directory in a file format.  Categories are simply folders/directories that are auto-created on the fly and dictated by the application category name.  Logs are simply files that are appended in each category directory with the same name.  This what scribe did and could not be repoduced by fluentd or flume, and thus, what scrawl still does today.  The default port is tcp 1201, but this can be configured to any port desired.  Recommed keeping your port binding above 1024 to avoid privilege escelation.   

Python
```
scrawl-v3.0/scrawld_tcp.py
```
```
>>> import asyncore
>>> from scrawl import scrawl
>>> tcp_server = scrawl.ScrawlTCPServer('0.0.0.0', 1201, '/scrawl')
>>> asyncore.loop()

```


Backlog and spooling
--------------------

There are two useful commands that are available for checking and sending the local spool directory, should there be any messages that get spooled for delivery.  However, keep in mind, that this is the job of the scrawld socket daemon.  The scrawld daemon was designed specifically to manage and send any such spooled messages.  Simply running the scrawld daemon will deliver out these messages automatically.

```
scrawl-v3.0/scrawl-backlog.py
scrawl-v3.0/scrawl-send-backlog.py 
```











