#!/usr/bin/python

__version__ = 'scrawltcp_tail:v0001'

import optparse
import os
import sys
import time

from collections import deque
Queue = deque()

debug = False

class Error(Exception): pass
class FileError(Error): pass

class Tail(object):

    def __init__(self, path, sleep=1.0, reopen_count=5):
        self.path = path
        self.sleep = sleep
        self.reopen_count = reopen_count

    def __iter__(self):
        while True:
            pos = self.file.tell()
            line = self.file.readline()
            if not line:
                self.wait(pos)
            else:
                yield line

    def open(self, tail=True):
        try:
            self.real_path = os.path.realpath(self.path)
            self.inode = os.stat(self.path).st_ino
        except OSError, error:
            raise FileError(error)
        try:
            self.file = open(self.real_path)
        except IOError, error:
            raise FileError(error)
        if tail:
            self.file.seek(0, 2)

    def close(self):
        try:
            self.file.close()
        except Exception:
            pass

    def reopen(self):
        self.close()
        reopen_count = self.reopen_count
        while reopen_count >= 0:
            reopen_count -= 1
            try:
                self.open(tail=False)
                return True
            except FileError:
                time.sleep(self.sleep)
        return False

    def check(self, pos):
        try:
            if self.real_path != os.path.realpath(self.path):
                return True
            stat = os.stat(self.path)
            if self.inode != stat.st_ino:
                return True
            if pos > stat.st_size:
                return True
        except OSError:
            return True
        return False

    def wait(self, pos):
        if self.check(pos):
            if not self.reopen():
                raise Error('Unable to reopen file: %s' % self.path)
        else:
            self.file.seek(pos)
            time.sleep(self.sleep)

def handle(path, category, host='127.0.0.1', port=1201, prefix='', postfix=''):

    from scrawl import scrawl
    client = scrawl.ScrawlTCPClient(host, port)
    tail = Tail(path)
    result = None

    try:
        tail.open()
    except Exception as e:
        print str(e)
        sys.exit(1) # unable to open file :-(

    for line in tail:
        line = line[:-1] # print str(line)
        message = prefix + line + postfix

        if len(Queue) > 0: # items on the Queue
            test = client.send("test", "test")
            if test is True:
                for item in list(Queue):
                    message = Queue.popleft()
                    result = client.send(category, message)
                    if debug: print 'Unloaded message from Queue'

        result = client.send(category, message)
        if debug: print 'Result: ' + str(result)
        if result is not True:
            Queue.append(message)
            if debug: print 'Queue message'


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option(
        '--file',
        dest='file',
        help='file to tail into Scribe',
        metavar='FILE',
    )
    parser.add_option(
        '--category',
        dest='category',
        help='Scribe category',
        metavar='CATEGORY',
    )
    parser.add_option(
        '--host',
        default='127.0.0.1',
        dest='host',
        help='destination Scrawl host server',
        metavar='HOST',
    )
    parser.add_option(
        '--port',
        default=1201,
        dest='port',
        help='destination Scrawl port',
        metavar='PORT',
        type='int',
    )
    parser.add_option(
        '--prefix',
        default='',
        dest='prefix',
        help='add to the beginning of each log line',
        metavar='PREFIX',
    )
    parser.add_option(
        '--postfix',
        default='',
        dest='postfix',
        help='add to the end of each log line',
        metavar='POSTFIX',
    )
    options, args = parser.parse_args()

    if options.file and options.category:
        try:
            handle(
                path=options.file,
                category=options.category,
                host=options.host,
                port=options.port,
                prefix=options.prefix,
                postfix=options.postfix,
            )
        except KeyboardInterrupt:
            sys.exit(0)
        except (Error), error:
            print >> sys.stderr, error
            sys.exit(1)
    else:
        parser.print_help()


