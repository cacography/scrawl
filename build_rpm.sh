#!/bin/bash

basedir=`dirname $0`

ver=`awk '/^Version: / {print $2}' $basedir/scrawld.spec`

mkdir -p ~/rpmbuild/{BUILD,RPMS,SOURCES,SPECS,SRPMS} >/dev/null 2>&1

if [ ! -f ~/rpmbuild/SOURCES/scrawl-$ver.tar.gz ]; then

  curl -k https://gitlab.com/cacography/scrawl/-/archive/master/scrawl-master.tar.gz >~/rpmbuild/SOURCES/scrawl-master.tar.gz
  tar xvf ~/rpmbuild/SOURCES/scrawl-master.tar.gz -C ~/rpmbuild/SOURCES/
  mv ~/rpmbuild/SOURCES/scrawl-master ~/rpmbuild/SOURCES/scrawl-$ver
  #tar cvfz ~/rpmbuild/SOURCES/scrawl-$ver.tar.gz ~/rpmbuild/SOURCES/scrawl-$ver
  cd ~/rpmbuild/SOURCES/
  tar cvfz scrawl-$ver.tar.gz scrawl-$ver
  cd -

fi


cp $basedir/scrawld.spec ~/rpmbuild/SPECS/scrawld.spec

rpmbuild -tb ~/rpmbuild/SOURCES/scrawl-$ver.tar.gz
#rpmbuild -ba ~/rpmbuild/SPECS/scrawld.spec

mkdir -p $basedir/package >/dev/null 2>&1
cp ~/rpmbuild/SRPMS/*.rpm $basedir/package/
#cp ~/rpmbuild/RPMS/x86_64/*.rpm $basedir/package/ >/dev/null 2>&1
cp ~/rpmbuild/RPMS/noarch/*.rpm $basedir/package/




