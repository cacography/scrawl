#!/usr/bin/env python

import os
import sys
import asyncore
from scrawl import scrawl

if __name__ == "__main__":

    if sys.platform == 'linux' or sys.platform == 'linux2':
        sys.path.insert(0, '/usr/libexec/scrawl')
        import config
        socket = config.scrawl['socket']
        remote = config.scrawl['host']
        port   = config.scrawl['port']

    elif sys.platform == 'darwin':
        #SIP https://en.wikipedia.org/wiki/System_Integrity_Protection
        #sys.path.insert(0, '/usr/libexec/scrawl') 
        socket = '/var/lib/scrawl/scrawld.sock'
        remote = 'localhost'
        port   = 1201

    elif sys.platform == 'win32':
        print("win32... not tried done yet.")
        sys.exit(1)

    if not os.path.exists("/var/lib/scrawl"):
        print "Missing: /var/lib/scrawl"
        sys.exit(1)

    if os.path.exists(socket):
        os.remove(socket)

    if not os.path.exists("/var/spool/scrawl/backlog"):
        print "Missing: /var/spool/scrawl/backlog"
        sys.exit(1)


    scrawld = scrawl.ScrawlSocketServerToScrawl(socket, remote, port)
    try:
        os.chmod(socket, 0777)
        asyncore.loop()
    finally:
        if os.path.exists(socket):
            os.unlink(socket)


