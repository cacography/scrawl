
# scrawl
__version__ = 'v3.0'

import sys
if sys.version_info[1] < 7:
    WARN =  'Warning: 100% cpu usage when using asyncore with UNIX socket '
    WARN += 'use python >= 2.7 https://bugs.python.org/issue12502 '
    print str(WARN)
import asyncore
import socket
import errno
import os
import stat
import math
import time
import threading
import multiprocessing

import logging
#DEBUG,INFO,WARNING,CRITICAL
logging.basicConfig(level=logging.INFO, format='%(levelname)s %(message)s')
log = logging.getLogger(__name__)

SPOOL = '/var/spool/scrawl'
#path  = '/var/lib/scrawl/scrawl.sock'
EVENTLOGS = '/scrawl'
SIZE = 1024
drain = True
debug = False

#NUL chr(0)
#SOH chr(1)  (start of heading)
#STX chr(2) (start of text)
#ETX chr(3) (end of text)
#EOT chr(4) (end of transmission)
#ENQ chr(5) (enquiry)
#ACK chr(6) (acknowledge)
#NAK chr(21) (negative acknowledge)

class ScrawlSocketClient():
    def __init__(self, path):
        self.path = path

    def is_socket(self, path):
        isSocket = False
        if os.path.exists(path):
            mode = os.stat(path).st_mode
            #import stat
            isSocket = stat.S_ISSOCK(mode)
            #isSocket = True
            #print "%s is socket: %s" % (path, isSocket)
        else:
            isSocket = False
        return isSocket

    def send(self, category, message):
        self.category = category
        self.message = message

        flag = '1' #SOH chr(1) (start of heading)
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

        if self.is_socket(self.path) == False:
            print 'Socket Error: No such file or directory ' + str(self.path)
            return False

        try:
            log.debug('connecting to ' + str(self.path))
            sock.connect(self.path)
        except socket.error, e:
            print('Socket Error: ' + str(e) + ' ' + str(self.path))
            return False

        catln = int(len(category))
        log.debug('catln is ' + str(catln))

        catbyte = chr(catln)
        log.debug('catbyte is ' + str(catbyte))

        msgln = int(len(message))
        log.debug('msgln is ' + str(msgln))

        msgbyte1 = chr(int(math.floor(msgln / 256)))
        log.debug('msgbyte1 is ' + str(msgbyte1) + '\r\n')

        msgbyte2 = chr(msgln % 256)
        log.debug('msgbyte2 is ' + str(msgbyte2) + '\r\n')

        header = str(flag) + str(catbyte) + str(msgbyte1) + str(msgbyte2)
        log.debug('scrawl header: ' + str(header))

        con = sock.recv(1)
        if not con.startswith('1'):
            log.critical('No scrawl header from server')
            return False

        msg = str(header) + str(category) + str(message)
        log.debug('sending message')
        sock.sendall(msg)
        reply = sock.recv(1)
        if not reply.startswith('6'): #ACK chr(6) (acknowledge)
            log.critical('No ACK reply from server ' + str(reply))
            success = False
        else:
            log.debug(str(reply))
            success = True

        return success

class ScrawlSocketServerToScrawl(asyncore.dispatcher):
    def __init__(self, path, scrawl_host, scrawl_port):
        self.path = path
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.set_reuse_addr()
        try:
            self.bind(self.path)
        except socket.error, e:
            log.debug(str(e) + ": " + str(self.path))
            sys.exit(1)

        # add backlog watch
        c = BackLogWatchScrawl()
        t = multiprocessing.Process(target=c.run, args=(1, self.scrawl_host, self.scrawl_port))
        t.start()

        self.listen(5)

    def handle_accept(self):
        client = self.accept()
        if client is None:
            pass
        else:
            log.debug('Socket connection on %s' % str(self.path))
            log.debug('scrawl_host ' + str(self.scrawl_host) + ' scrawl_port ' + str(self.scrawl_port))
            handler = ScrawlHandlerSocketMemToScrawl(*client, scrawl_host=self.scrawl_host, scrawl_port=self.scrawl_port)

class BackLogWatchScrawl:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, n, scrawl_host, scrawl_port):

        CHECK_INTERVAL = 10

        success = None
        result  = None
        while self._running and n > 0:
            #sys.stdout.write("  \b%s" % n)
            #sys.stdout.flush()

            #check if backlog exists...
            scrawl_backlog_dir = '/var/spool/scrawl/backlog'
            dirList = os.listdir(scrawl_backlog_dir)
            backLog = []
            for category in dirList:
                scrawl_spool_dir_file = scrawl_backlog_dir + '/' + category + '/' + category + '_00000'
                if os.path.isfile(scrawl_spool_dir_file):
                    if os.stat(scrawl_spool_dir_file).st_size == 0:
                        os.unlink(scrawl_spool_dir_file)
                        log.debug('removed empty file ' + str(scrawl_spool_dir_file))
                    else:
                        backLog.append(scrawl_spool_dir_file)

            if backLog:

                for backlogfile in backLog:
                    log.debug(backlogfile)

                    category = 'scrawl'
                    message  = 'scrawl BackLogWatchScrawl'
                    #client = ScrawlClientTCP(scrawl_host, scrawl_port)
                    client = ScrawlTCPClient(scrawl_host, scrawl_port)
                    try:
                        result = client.send(category, message)
                    except:
                        #party.foul
                        #log.debug('scribe TTransportException' + str(result))
                        log.debug('scrawl TCP client error' + str(result))

                    if result is True:
                        success = 'success'
                        # deliver dir/file...
                        if os.path.isfile(backlogfile):
                            log.debug('proto send scrawl ' + str(scrawl_host) + ' port ' + str(scrawl_port))
                            drain = ScrawlSpoolDirFileToScrawlRetryMemTimeOutSaveBack(backlogfile, scrawl_host, scrawl_port)
                            try:
                                send = drain.send()
                            except IOError as e:
                                log.debug('IOError ' + str(e))
            else:
                log.debug('backLog empty')

            log.debug('scrawl backlog run again in ' + str(CHECK_INTERVAL) + ' ...')
            #n += 1
            time.sleep(CHECK_INTERVAL)

class ScrawlSpoolDirFileToScrawlRetryMemTimeOutSaveBack():
    # dir format storage.
    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                log.debug('REMOVE file ' + str(self.path))
                os.remove(self.path)
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        log.debug(' get cat from ' + self.path)

        infile = self.path
        osdir1  = os.path.dirname(infile)
        osdir2  = os.path.dirname(os.path.dirname(infile))

        log.debug(infile + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        drainDict = {}
        count = 0
        for message in mem_data:
            count += 1
            drainDict[count] = message

        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TTS-minus scrawl TCP delivery... ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                client = ScrawlTCPClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    #party.foul
                    #log.warning('scribe TTransportException ' + str(result))
                    log.warning('scrawl TCP result ' + str(result))

                if result is True:
                    log.debug('ScrawlClient return ' + str(result))
                    n -= 1
                    del drainDict[k]
            TIMEOUT -= 1
            time.sleep(1)

        #unload drainDict
        #for k in list(drainDict.keys()):
        #    open('/tmp/out', 'a').write(drainDict[k])
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])

        backlogdir = SPOOL + '/backlog'
        if not os.path.isdir(backlogdir):
            os.mkdir(backlogdir, 0755 )
            log.debug('mkdir ' + str(backlogdir))

        writedir = backlogdir + '/' + category
        if not os.path.isdir(writedir):
            os.mkdir(writedir, 0755 )
            log.debug('mkdir ' + str(writedir))
        writefile = writedir + '/' + category + '_00000'

        for k in list(drainDict.keys()):
            with open(writefile, 'a') as fout:
                #fout.write(message + '\r\n')
                fout.write(drainDict[k])
                log.debug('writefile ' + str(writefile))

        return result


class ScrawlHandlerSocketMemToScrawl(asyncore.dispatcher_with_send):
    def __init__(self, sock, addr, scrawl_host, scrawl_port):
        self.scrawl_host = scrawl_host
        self.scrawl_port = scrawl_port
        asyncore.dispatcher_with_send.__init__(self, sock)
        #self.buffer = 'scrawl'
        self.buffer = '1' #SOH chr(1)  (start of heading)
        log.debug("ScrawlHandlerSocketMemToScrawlTCP")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            # v2.4
            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))

            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #response = 'OK'
            response = '6' #ACK chr(6) (acknowledge)
            self.send(response)
            log.debug('send ' + str(response))

            #drain
            #scrawl.out w/ timeout and save back
            drain = WatchMemTaskSyncToScrawlRetryMemTimeOutSaveBack()
            t = threading.Thread(target=drain.run, args=(category, message, self.scrawl_host, self.scrawl_port))
            t.start()

            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.send(self.buffer)
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close scrawl_host")

class WatchMemTaskSyncToScrawlRetryMemTimeOutSaveBack:
    def __init__(self):
        self._running = True

    def terminate(self):
        self._running = False

    def run(self, category, message, host, port):
        category = category
        message = message
        scrawl_host = host
        scrawl_port = port
        drain = ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack(category, message, scrawl_host, scrawl_port)
        send = drain.send()
        log.debug(send)


class ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack():
    # dir format storage.
    def __init__(self, category, message, host, port):
        self.category = category
        self.message = message
        self.host = host
        self.port = port
        log.debug('category ' + str(category))
        log.debug('message ' + str(message))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))
        log.debug('ScrawlSpoolMemFileToScrawlTCPRetryMemTimeOutSaveBack')

    def send(self):
        log.debug('category :' + self.category)
        log.debug('message :' + self.message)

        drainDict = {}
        drainDict[self.category] = self.message

        TIMEOUT = 1
        result = None
        while TIMEOUT > 0:
            log.debug('TTS-minus scrawl ' + str(TIMEOUT))
            client = ScrawlTCPClient(self.host, self.port)
            try:
                result = client.send(self.category, self.message)
                log.debug('ScrawlTCPClient retun ' + str(result))
            except:
                #party.foul
                #log.warning('scrawl TTransportException ' + str(result))
                log.warning('scrawl error/return ' + str(result))

            if result is None:
                log.debug('Scrawl Client return FAIL ' + str(result))
                #del drainDict[self.category]
                #break
            if result is True:
                log.debug('Scrawl Client return SUCCESS ' + str(result))
                del drainDict[self.category]
                break
            TIMEOUT -= 1
        # check

        if not drainDict.keys():
            log.debug("DELIVERED scrawl TCP v2.4 ")
        else:
            log.debug("WRITE BACKLOG scrawl TCP v2.4")
            backlogdir = SPOOL + '/backlog'
            if not os.path.isdir(backlogdir):
                os.mkdir(backlogdir, 0755 )
                log.debug('mkdir ' + str(backlogdir))

            writedir = backlogdir + '/' + self.category
            if not os.path.isdir(writedir):
                os.mkdir(writedir, 0755 )
                log.debug('mkdir ' + str(writedir))
            writefile = writedir + '/' + self.category + '_00000'

            with open(writefile, 'a') as fout:
                fout.write(self.message + '\r\n')
                log.debug('writefile ' + str(writefile))

        return result


class ScrawlTCPServer(asyncore.dispatcher):
    def __init__(self, host, port, out):
        self.out = out
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.set_reuse_addr()
        self.bind((host, port))
        self.listen(5)

    def handle_accept(self):
        pair = self.accept()
        if pair is not None:
            sock, addr = pair
            log.debug('Incoming connection from %s' % repr(addr))
            handler = ScrawlHandlerSpoolDirTCP(sock, self.out)


class ScrawlHandlerSpoolDirTCP(asyncore.dispatcher_with_send):
    def __init__(self, sock, EVENTLOGS):
        self.EVENTLOGS = EVENTLOGS
        asyncore.dispatcher_with_send.__init__(self, sock)
        self.buffer = '1'
        log.debug("ScrawlHandlerSpoolDirTCP v2.5")

    def handle_read(self):
        log.debug("handle_read")

        data = self.recv(1024)
        if data:
            log.debug('getting data...')
            log.debug(str(type(data)))

            byte1 = flag = data[0:1]
            byte2 = catsz1 = data[1:2]
            byte3 = msgsz1 = data[2:3]
            byte4 = msgsz2 = data[3:4]

            catsz_ord = ord(str(byte2))

            log.debug("catsz_ord  (cat_size) " + str(catsz_ord))

            catsz_size = int(catsz_ord)
            log.debug('catsz_size ' + str(catsz_size))

            category = data[4:catsz_size + 4]
            log.debug('category: ' + category)

            msglnstr = str(ord(str(byte3))) + str(ord(str(byte4)))
            log.debug('msgln str is ' + str(msglnstr))

            msgln = int(msglnstr)
            log.debug('msgln int is ' + str(msgln))

            message = data[4 + catsz_size:4 + catsz_size + msgln]
            log.debug('message: ' + message)

            #print 'start'
            writedir = self.EVENTLOGS + '/' + category
            if not os.path.isdir(writedir):
                #print 'mkdir'
                try:
                    os.mkdir(writedir, 0755)
                    log.debug('mkdir ' + str(writedir))
                except OSError as e:
                    print 'Fail! mkdir ' + str(writedir)
                    print str(e)
                    sys.exit(1)

            writefile = writedir + '/' + category + '_00000'

            try:
                with open(writefile, 'a') as wfile:
                    wfile.write(message + '\r\n')
                    log.debug('writefile ' + str(writefile))
            except IOError as e:
                print 'Fail! write ' + str(writefile)
                print str(e)
                sys.exit(1)

            response = '6' #response = 'OK'
            self.send(response)
            log.debug('send ' + str(response))
            return response

    def writable(self):
        return (len(self.buffer) > 0)

    def handle_write(self):
        self.buffer = ''

    def handle_close(self):
        self.close()
        log.debug("handle_close")


class ScrawlTCPClient():
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def send(self, category, message):
        success = None
        self.category = category
        self.message = message

        if debug: print('client_send category ' + str(category))
        if debug: print('client_send message ' + str(message))

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if debug: print('socket.time')
        try:
            if debug: print('scrawl protocol connecting to ' + self.host + ' port ' + str(self.port))
            server_address = (self.host, self.port)
            sock.connect(server_address)
        except socket.gaierror, e:
            print('Address-related error: ' % str(e) + ' ' + str(self.host) + ':' + str(self.port))
            return False
        except socket.error, e:
            print('Connection error: ' + str(e) + ' ' + str(self.host) + ':' + str(self.port))
            return False

        try:
            flag = '1'
            if debug: print('flag set to 1')

            catln = int(len(category))
            if debug: print('catln is ' + str(catln))

            catbyte = chr(catln)
            if debug: print('catbyte is ' + str(catbyte))

            msgln = int(len(message))
            if debug: print('msgln is ' + str(msgln))

            msgbyte1 = chr(int(math.floor(msgln / 256)))
            if debug: print('msgbyte1 is ' + str(msgbyte1) + '\r\n')

            msgbyte2 = chr(msgln % 256)
            if debug: print('msgbyte2 is ' + str(msgbyte2) + '\r\n')

            header = str(flag) + str(catbyte) + str(msgbyte1) + str(msgbyte2)
            if debug: print('scrawl header: ' + str(header))


            msg = str(header) + str(category) + str(message)
            if debug: print('sending scrawl msg')
            sock.sendall(msg)
            reply = sock.recv(1024)

            if not reply.startswith('6'):
                print('No ACK reply from server ' + str(reply))
                success = False
            else:
                if debug: print('scrawl ' + str(reply))
                success = True

            if debug: print('ScrawlTCPClient v2.5')

        finally:
            sock.close()
            if debug: print('sock.close')
            return success


class ScrawlSpoolDirFileToScrawlTCPRetryMemTimeOutSaveBack():
    # dir format storage.
    # read-in entire file, truncate
    # save-back

    def __init__(self, path, host, port):
        self.path = path
        self.host = host
        self.port = port
        log.debug('path ' + str(path))
        log.debug('host ' + str(host))
        log.debug('port ' + str(port))

    def send(self):
        success = None
        with open(self.path, 'r+') as fin:
            mem_data = fin.read().splitlines(True)
            if drain:
                #fin.truncate(0)
                #log.debug("TRUNCATE " + str(self.path))
                log.debug("REMOVE " + str(self.path))
                os.remove(self.path)
        #with open(self.path, 'w') as fout:
        #    fout.writelines(mem_data[1:])
        try:
            first_line = mem_data[0]
        except IndexError as e:
            print 'Empty file: ' + str(self.path)
            sys.exit(1)
            #return False

        log.debug(' get cat from ' + self.path)

        osdir1  = os.path.dirname(self.path)
        osdir2  = os.path.dirname(os.path.dirname(self.path))

        log.debug(self.path + ' ' + osdir1 + ' ' + osdir2)
        #osdir1
        #DEBUG  get cat from /var/spool/scrawl/my_test v2.2/my_test v2.2_00000
        #/var/spool/scrawl/my_test v2.2/my_test v2.2_00000 /var/spool/scrawl/my_test v2.2 /var/spool/scrawl

        path_list = osdir1.split(os.sep)
        log.debug(path_list)
        enditem = path_list[-1]

        category = enditem
        catsz_size = str(len(category))

        log.debug('category :' + category)
        log.debug('catsz_size :' + catsz_size)

        #working.2
        count = 0
        drainDict = {}
        #retryList = []
        for message in mem_data:
            count += 1
            drainDict[count] = message

        TIMEOUT = 3
        result = None
        n = len(drainDict.keys())
        while drainDict and TIMEOUT > 0:
            log.debug('TTS-minus scrawl ' + str(TIMEOUT))
            for k in list(drainDict.keys()):
                #client = ScribeClient(self.host, self.port)
                client = ScrawlTCPClient(self.host, self.port)
                try:
                    message = drainDict[k]
                    result = client.send(category, message)
                except:
                    #log.warning('scribe TTransportException ' + str(result))
                    #log.critical('scribe TTransportException ')
                    log.critical('log.critical ' + str(result))
                log.debug('RESULT is ' + str(result))
                #if result == 'OK':
                if result == True:
                    n -= 1
                    del drainDict[k]
                    log.debug('scrawl OK')
                else:
                    #log.debug('ScribeClient return ' + str(result))
                    log.debug('Not OK.  scrawl queue store message ...')
            TIMEOUT -= 1
            time.sleep(1)

        #write backlog
        backlogdir = SPOOL + '/backlog'
        if not os.path.isdir(backlogdir):
            os.mkdir(backlogdir, 0755 )
            log.debug('mkdir ' + str(backlogdir))

        writedir = backlogdir + '/' + category
        if not os.path.isdir(writedir):
            os.mkdir(writedir, 0755 )
            log.debug('mkdir ' + str(writedir))
        writefile = writedir + '/' + category + '_00000'

        for k in list(drainDict.keys()):
            with open(writefile, 'a') as fout:
                #fout.write(message + '\r\n')
                fout.write(drainDict[k])
                log.debug('writefile ' + str(writefile))


        return result


