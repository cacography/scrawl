#!/usr/bin/python

import sys
from scrawl import scrawl

if __name__ == "__main__":

    category = 'ScrawlTCPClient'
    message  = 'a ScrawlTCPClient message ...'

    client = scrawl.ScrawlTCPClient('localhost', 1201)
    result = client.send(category, message)

    if result is True:
        print str(result)
        sys.exit(0)
    else:
        print str(result)
        sys.exit(1)

