#!/usr/bin/python

import sys
from scrawl import scrawl

if __name__ == "__main__":

    if sys.argv[1:]:
        category = sys.argv[1]
        message  = sys.argv[2]
    else:
        category = 'TESTING scrawl 321'
        message  = 'yep, this is a message, scrawld...'

    client = scrawl.ScrawlSocketClient("/var/lib/scrawl/scrawld.sock")
    result = client.send(category, message)

    if result is True:
        print str(result)
        sys.exit(0)
    else:
        print str(result)
        sys.exit(1)


