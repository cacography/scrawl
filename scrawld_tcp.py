#!/usr/bin/python

import asyncore
from scrawl import scrawl

if __name__ == "__main__":

    tcp_server = scrawl.ScrawlTCPServer('0.0.0.0', 1201, '/scrawl')
    asyncore.loop()

